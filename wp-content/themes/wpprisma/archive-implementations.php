<?php
/*
Template Name: Страница реализаций
 */
?>
<?php get_header();?>
    <div class="main">
        <a href=".header" class="scroll-top"></a>

        <?php (new Breadcrums())->render();?>
        <?php (new About_Stage())->render(3);?>
        <?php (new Archive_Implementations())->render();?>
        <?php (new Main_Page_News())->render();?>
    </div>

<?php get_footer();?>