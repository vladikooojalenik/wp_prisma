<?php

get_header();?>
<div class="main">
<a href=".header" class="scroll-top"></a>

<?php (new Breadcrums())->render();?>
<?php (new Articles())->render();?>
<?php (new Main_Page_Implementations())->render();?>
<?php (new Main_Page_News())->render();?>
</div>
<?
get_footer();
?>
