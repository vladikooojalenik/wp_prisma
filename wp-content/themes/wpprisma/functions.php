<?php

define('TEMPLATE_PATH', get_template_directory_uri() . '/');

function create_post_type()
{
    register_post_type('news',
        array(
            'labels' => array(
                'name' => ('News'),
                'singular_name' => ('News'),
            ),
            'public' => true,
            'taxonomies' => array('category'),
            'has_archive' => true,
            'supports' => array('title', 'editor', 'thumbnail', 'post-formats', 'post-thumbnails'),
        )
    );

    register_post_type('activities',
        array(
            'labels' => array(
                'name' => ('Activities'),
                'singular_name' => ('Activities'),
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('title', 'editor', 'thumbnail', 'post-formats', 'post-thumbnails'),
        )
    );

    register_post_type('implementations',
        array(
            'labels' => array(
                'name' => ('Implementations'),
                'singular_name' => ('Implementations'),
            ),
            'public' => true,
            'taxonomies' => array('category'),
            'has_archive' => true,
            'supports' => array('title', 'editor', 'thumbnail', 'post-formats', 'post-thumbnails'),
        )
    );

    register_post_type('request',
        array(
            'labels' => array(
                'name' => ('Request'),
                'singular_name' => ('Request'),
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('title', 'editor', 'thumbnail', 'post-formats', 'post-thumbnails'),
        )
    );

    register_post_type('technology',
        array(
            'labels' => array(
                'name' => ('Technology'),
                'singular_name' => ('Technology'),
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('title', 'editor', 'thumbnail', 'post-formats', 'post-thumbnails'),
        )
    );
}
add_action('init', 'create_post_type');

function wpprisma_setup()
{

    add_theme_support('title-tag');

    add_theme_support('custom-logo', array('height' => '90px', 'width' => '130px', 'flex-height' => true));

    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(218, 218);

    add_theme_support('html5', array('search-form', 'comment-form', 'comment-list', ' gallery', 'caption'));

    add_theme_support('post-formats', array('aside', 'image', 'video', 'gallery'));

    register_nav_menu('primary', 'Primary menu');

}

add_action('after_setup_theme', 'wpprisma_setup');

add_action('widgets_init', 'my_register_widgets');

function my_register_widgets()
{
    register_widget('Widget_Header');
    register_widget('Widget_Footer');
}

register_sidebar('sidebar', 'header');
register_sidebar('Footer', 'footer');

function wpprisma_scripts()
{
    wp_enqueue_style('style-css', get_stylesheet_uri());
    wp_enqueue_style('dev_1', get_template_directory_uri() . '/styles/dev_1.css');
    wp_enqueue_style('fancybox', get_template_directory_uri() . '/styles/fancybox.css');
    wp_enqueue_style('formstyler', get_template_directory_uri() . '/styles/formstyler.css');
    wp_enqueue_style('normalize', get_template_directory_uri() . '/styles/normalize.css');
    wp_enqueue_style('slick', get_template_directory_uri() . '/styles/slick.css');
}

add_action('wp_enqueue_scripts', 'wpprisma_scripts');

function prisma_question()
{
    $recepient = "vasyapupkin1999uk@gmail.com";
    $sitename = "Prisma";

    $name = trim(filter_input(INPUT_POST, "questionFormName"));
    $phone = trim(filter_input(INPUT_POST, "questionFormPhone"));
    $email = trim(filter_input(INPUT_POST, "questionFormEmail"));
    $text = trim(filter_input(INPUT_POST, "questionFormMessage"));
    $message = "Имя: $name \nТелефон: $phone \nEmail: $email \nСообщение: $text";

    $pagetitle = "Новая заявка с сайта \"$sitename\"";
    mail($recepient, $pagetitle, $message);

    $post_data = array(
        'post_title' => ($pagetitle),
        'post_content' => ($message),
        'post_status' => 'publish',
        'post_author' => 1,
        'post_type' => 'request',
    );

    $post_id = wp_insert_post($post_data);

    echo 'true';
    die;
}
add_action("wp_ajax_prisma_question", "prisma_question");
add_action("wp_ajax_nopriv_prisma_question", "prisma_question");

add_filter('wpseo_breadcrumb_single_link', 'wpseo_change_breadcrumb_link', 10, 2);
function wpseo_change_breadcrumb_link($link_output, $link)
{
    $text_to_change = 'Implementations';
    if ($link['text'] == $text_to_change) {
        $link_output = 'Все проекты';
    }
    $text_to_change = 'Activities';
    if ($link['text'] == $text_to_change) {
        $link_output = '';
    }
    $text_to_change = 'Technology';
    if ($link['text'] == $text_to_change) {
        $link_output = '';
    }
    return $link_output;

}

class Theme_AutoLoader
{
    public static function init()
    {

        self::load(__DIR__ . '/tamplate-parts');
        self::load(__DIR__ . '/includes');
    }

    public static function load($path)
    {
        $path = $path . '/';
        $path = preg_replace('/\/+$/', '/', $path);

        $files = array_diff(scandir($path), ['.', '..']);

        if (!$files) {
            return;
        }

        foreach ($files as $file) {
            if (is_dir($path . $file)) {
                self::load($path . $file);
            } elseif (preg_match('/(\.php)$/', $file)) {
                require_once $path . $file;
            }
        }
    }
}

Theme_AutoLoader::init();
