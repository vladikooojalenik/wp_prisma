<?php
class Breadcrums
{
    public function __construct()
    {
    }

    public function render()
    {
        ?><div class="cg breadcrumbs"><?
        $this->get_render();
        ?></div><?
    }
    public function get_render()
    {
        if(function_exists('yoast_breadcrumb')):
            yoast_breadcrumb('<a id="breadcrumbs" class="breadcrumbs-link">,</a>');
        endif;
    }


}