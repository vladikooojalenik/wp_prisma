<?php
class Main_Page_Activities
{
    public function __construct()
    {
    }

    public function render()
    {?>
       <section class="cg main-pass">
            <div class="link-wrap">
                <a href="#" class="main-implement__link main-pass__ttl">Направления деятельности</a>
            </div>

            <div class="main-pass__blocks">
              <?php
$args = array('post_type' => 'activities', 'posts_per_page' => 9);
        $the_query = new WP_Query($args);
        ?>

              <?php if ($the_query->have_posts()): ?>
              <?php while ($the_query->have_posts()): $the_query->the_post();?>
										                <a href="#" class="main-pass__block">
										                    <div class="pass-left">
										                        <div class="pass-left__wrap">
										                            <img src="<?php echo get_the_post_thumbnail_url() ?>" alt="Pass image" class="pass-img">
										                        </div>
										                    </div>
										                    <div class="pass-right">
										                        <span class="pass-right__ttl"><?php the_title()?></span>
										                        <span class="pass-right__txt"><?php the_content();?> </span>
										                    </div>
										                </a>
										                <?php endwhile;?>
                    <?php endif;?>
                </section>
                <?php
}
}