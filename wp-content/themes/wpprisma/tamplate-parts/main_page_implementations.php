<?php
class Main_Page_Implementations
{
    public function __construct()
    {
    }

    public function render()
    {?>
       <section class="cg mb-distance-1">
            <div class="link-wrap">
                <a href="#" class="main-implement__link">Реализации</a>
            </div>
            <div class="main-implement__blocks">
              <?php
$args = array('post_type' => 'implementations', 'posts_per_page' => 8);
        $the_query = new WP_Query($args);
        ?>

              <?php if ($the_query->have_posts()): ?>
              <?php while ($the_query->have_posts()): $the_query->the_post();?>
											              <div class="main-implement__block">
											                    <a href="<?php the_permalink()?>" class="block-link">
											                        <div class="block-img__wrap">
											                            <img src="<?php echo get_the_post_thumbnail_url() ?>" alt="Block image" class="block-img">
											                            <img src="<?=TEMPLATE_PATH?>img/icon/imeplement-icon__photo.png" alt="Post icon" class="implement-post__icon photo-icon">
											                        </div>
											                        <span class="block-date"><?php the_time('j F Y');?></span>
				                                                    <span class="block-ttl"><?php the_title();?></span>
				                                                    <?php $post_id = get_the_ID();
            $post_category = wp_get_post_terms($post_id, 'category', ['fields' => 'names']);
            $categories = implode('</span></div><div class="posts__block-mark"><span>', $post_category);?>
											                        <span class="block-tag"><?php echo $categories ?></span></span>
											                    </a>
											                </div>
															 <?php endwhile;?>
                    <?php endif;?>
                </section>
                <?php
}
}