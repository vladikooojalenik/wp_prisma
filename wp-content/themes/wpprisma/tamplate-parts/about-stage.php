<?php
class About_Stage
{
    public function __construct()
    {
        $this->widget = wp_get_sidebars_widgets();
        $widget_id = $this->widget["sidebar-2"][0];
        $this->menu = get_field("menu", 'widget_' . $widget_id);
    }

    public function render($page_id)
    {
        ?>
    <section class="cg bc about-content">
        <div class="tab-nav about-content__nav">
        <?
        $value = $this->menu[$page_id]["submenus"];
        foreach($value as $elem)
        {
            if(stristr($elem["link"]["url"],$_SERVER['REQUEST_URI']))
            {?>
            <a class="tab-nav__link about-nav__link tab-active" href="<? echo $elem["link"]["url"]?>"><? echo $elem["link"]["title"]?></a>
            <? }else
            {?>
            <a class="tab-nav__link about-nav__link" href="<? echo $elem["link"]["url"]?>"><? echo $elem["link"]["title"]?></a>
       <?}}?>
        </div>
    </section>
                <?php
}
}