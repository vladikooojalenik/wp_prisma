<?php
class News
{
    public function __construct()
    {
    }

    public function render()
    {?>
        <section class="cg news">
            <div class="bc-open">
                <?
                the_title();
                the_content();
                ?>
                <img src="<?php the_post_thumbnail_url();?>">
            </div>
    </section>

        <?php
}
}