<?php
class Implementation
{
    public function __construct()
    {
    }

    public function render()
    {?>
      <section class="cg implementation">
            <div class="bc-open">
            <?
                the_title();
                the_content();
                ?>
                <img src="<?php the_post_thumbnail_url();?>">
            </div>
    </section>
        <?php
}
}