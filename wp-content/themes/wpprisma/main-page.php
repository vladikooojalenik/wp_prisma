<?php
/*
Template Name: Главная страница
 */
get_header();?>
    <!-- /HEADER -->
    <div class="main">
        <a href=".header" class="scroll-top"></a>

        <?php (new Main_Page_Slider())->render();?>
        <?php (new Main_Page_Intro())->render();?>
        <?php (new Main_Page_Activities())->render();?>
        <?php (new Main_Page_Implementations())->render();?>
        <?php (new Main_Page_News())->render();?>
    </div>

    <!-- FOOTER -->
    <?php get_footer();?>
    <!-- /FOOTER -->
