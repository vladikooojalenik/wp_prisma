<?php
class Widget_Footer extends WP_Widget
{

    public function __construct()
    {
        // Запускаем родительский класс
        parent::__construct(
            'footer', // ID виджета, если не указать (оставить ''), то ID будет равен названию класса в нижнем регистре: my_widget
            'Footer',
            array('description' => 'Описание виджета')
        );

        // стили скрипты виджета, только если он активен
        if (is_active_widget(false, false, $this->id_base) || is_customize_preview()) {
            add_action('wp_enqueue_scripts', array($this, 'add_my_widget_scripts'));
            add_action('wp_head', array($this, 'add_my_widget_style'));
        }
    }

// Вывод виджета
    public function widget($args, $instance)
    {
        $this->info_copy = get_field("info_copy", 'widget_' . $args['widget_id']);
        $this->info_protect = get_field("info_protect", 'widget_' . $args['widget_id']);
        $this->question_button = get_field("question_button", 'widget_' . $args['widget_id']);
        $this->contact_address = get_field("contact_address", 'widget_' . $args['widget_id']);
        $this->address_link = get_field("address_link", 'widget_' . $args['widget_id']);
        $this->design_img = get_field("design_img", 'widget_' . $args['widget_id']);
        $this->contact_mail = get_field("contact_mail", 'widget_' . $args['widget_id']);
        $this->contact_tel_1 = get_field("contact_tel_1", 'widget_' . $args['widget_id']);
        $this->contact_tel_2 = get_field("contact_tel_2", 'widget_' . $args['widget_id']);
        ?>


<div class="modal" id="myModal">
        <div class="modal-content question-content" id="modal-question">
            <span class="close"></span>

            <span class="question-ttl">Задать вопрос</span>
            <span class="question-txt">Спасибо! Мы обязательно свяжемся с Вами и ответям на все Ваши вопросы</span>
            <form class="question-form" action="<?=admin_url('admin-ajax.php')?>">
            <input type="hidden" name="action" value="prisma_question">

                <div class="contact-form-row cfix">
                    <div class="contact-form-item">
                        <div class="contact-form-item-input form_row">
                            <div class="form_input question-inp">
                                <input class="inp" type="text" required name="questionFormName" placeholder="Ваше имя*">
                            </div>
                        </div>
                    </div>
                    <div class="contact-form-item">
                        <div class="contact-form-item-input form_row">
                            <div class="form_input question-inp">
                                <input class="inp" type="tel" required name="questionFormPhone" placeholder="Номер телефона*">
                            </div>
                        </div>
                    </div>
                    <div class="contact-form-item">
                        <div class="contact-form-item-input form_row">
                            <div class="form_input question-inp">
                                <input class="inp" type="email" required name="questionFormEmail" placeholder="Email*">
                            </div>
                        </div>
                    </div>
                    <textarea class="txt-area qusetion-txtarea" name="questionFormMessage" placeholder="Сообщение"></textarea>
                    <input class="form-btn question-btn" type="submit" value="Отправить">
                </div>
            </form>
        </div>

        <div class="modal-content success-content" id="modal-success">
            <span class="close"></span>

            <span class="success-ttl">Спасибо!</span>
            <span class="success-txt">Ваша заявка принята в обработку</span>
            <span class="success-icon"></span>
        </div>
    </div>

    <footer class="footer">

<div class="cg f-menu">
    <div class="f-menu__info">
        <span class="info-copy"><?=$this->info_copy?></span>
        <span class="info-protect"><?=$this->info_protect?></span>
    </div>
    <div class="question-wrap">
        <a href="javascript:void(0);" data-bid="modal-question" class="f-menu__question myBtn"><?=$this->question_button?></a>
    </div>
    <div class="f-menu__contacts">
        <a href="<?=$this->address_link?>" target="_blank" class="contact-link contact-address"><?=$this->contact_address?></a>
        <?php $this->contact_phones_first($this->contact_tel_1);?>
        <?php $this->contact_phones_second($this->contact_tel_2);?>
        <a href="mailto:<?=$this->contact_address?>" class="contact-link contact-mail"><?=$this->contact_address?></a>
    </div>
    <a href="https://mgn.com.ua/" target="_blank" class="f-menu__design">
        <span class="design-txt">дизайн сайта</span>
        <img src="<?=$this->design_img?>" alt="Dising logotype" class="design-img">
    </a>
</div>
</footer>


</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<!-- build:js scripts/validate_script.js -->
<script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/scripts/validate_script.js" ></script>
<!-- endbuild -->

<!-- build:js scripts/plagins.js -->
<script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/scripts/plagins/device.js" ></script>
<script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/scripts/plagins/jquery.fancybox.min.js" ></script>
<script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/scripts/plagins/jquery.formstyler.min.js" ></script>
<script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/scripts/plagins/jquery.validate.min.js" ></script>
<script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/scripts/plagins/maskInput.js" ></script>
<script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/scripts/plagins/slick.js" ></script>
<!-- add you plagins js here -->

<!-- endbuild -->

<!-- build:js scripts/main.js -->
<script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/scripts/basic_scripts.js" ></script>
<script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/scripts/develop/develop_1.js" ></script>
</body>
<?php wp_footer();?>
</html>

    <?php
}

// Сохранение настроек виджета (очистка)
    public function update($new_instance, $old_instance)
    {
    }

// html форма настроек виджета в Админ-панели
    public function form($instance)
    {
    }

// скрипт виджета
    public function add_my_widget_scripts()
    {
        // фильтр чтобы можно было отключить скрипты
        if (!apply_filters('show_my_widget_script', true, $this->id_base)) {
            return;
        }

        $theme_url = get_stylesheet_directory_uri();

        wp_enqueue_script('my_widget_script', $theme_url . '/my_widget_script.js');
    }

// стили виджета
    public function add_my_widget_style()
    {
        // фильтр чтобы можно было отключить стили
        if (!apply_filters('show_my_widget_style', true, $this->id_base)) {
            return;
        }

        ?>
    <style>
        .my_widget a{ display:inline; }
    </style>
    <?php
}

    public function contact_phones_first($contact_tel_1)
    {
        if ($contact_tel_1) {
            $phones = explode("\n", $contact_tel_1);
            ?><div class="contact-phones__first"><?
        foreach($phones as $phone)
        {
            ?><a href="tel:+<?=preg_replace('/[^\d]/', '', $phone);?>" class="contact-link"><?=$phone?></a><?
        }
    ?></div><?php
}
    }

    public function contact_phones_second($contact_tel_2)
    {
        if ($contact_tel_2) {
            $phones = explode("\n", $contact_tel_2);
            ?><div class="contact-phones__second"><?
        foreach($phones as $phone)
        {
            ?><a href="tel:+<?=preg_replace('/[^\d]/', '', $phone);?>" class="contact-link"><?=$phone?></a><?
        }
    ?></div><?php
}
    }

}