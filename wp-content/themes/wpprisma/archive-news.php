<?php
/*
Template Name: Страница новостей
 */
?>
<?php get_header();?>
    <div class="main">
        <a href=".header" class="scroll-top"></a>

        <?php (new Breadcrums())->render();?>
        <?php (new Archive_News())->render();?>
        <?php (new Main_Page_Implementations())->render();?>
    </div>

<?php get_footer();?>