<?php
/*
Template Name: Страница статей
 */
?>
<?php get_header();?>
    <div class="main">
        <a href=".header" class="scroll-top"></a>

        <?php (new Breadcrums())->render();?>
        <?php (new Archive_Articles())->render();?>
        <?php (new Main_Page_Implementations())->render();?>
        <?php (new Main_Page_News())->render();?>
    </div>

<?php get_footer();?>