<?php

get_header();?>
<div class="main">
<a href=".header" class="scroll-top"></a>

<?php (new Breadcrums())->render();?>
<?php (new News())->render();?>
<?php (new Main_Page_Implementations())->render();?>
</div>
<?
get_footer();
?>
