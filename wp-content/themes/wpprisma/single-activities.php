<?php
get_header();?>

<div class="main">
        <a href=".header" class="scroll-top"></a>

        <?php (new Breadcrums())->render();?>
        <?php (new About_Stage())->render(1);?>
        <?php (new Activities())->render();?>
        <?php (new Main_Page_Implementations())->render();?>
        <?php (new Main_Page_News())->render();?>
</div>
<?
get_footer();
?>