<?php

/*
Template Name: Инфоблок
 */

get_header();?>


<div class="main">
        <a href=".header" class="scroll-top"></a>

        <?php (new Breadcrums())->render();?>
        <?php (new About_Stage())->render(0);?>
        <?php the_content();?>
        <?php (new Main_Page_Implementations())->render();?>
        <?php (new Main_Page_News())->render();?>

    </div>

    <?php get_footer();?>