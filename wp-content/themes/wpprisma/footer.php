
<?php dynamic_sidebar('sidebar-1');?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<!-- build:js scripts/validate_script.js -->
<script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/scripts/validate_script.js" ></script>
<!-- endbuild -->

<!-- build:js scripts/plagins.js -->
<script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/scripts/plagins/device.js" ></script>
<script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/scripts/plagins/jquery.fancybox.min.js" ></script>
<script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/scripts/plagins/jquery.formstyler.min.js" ></script>
<script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/scripts/plagins/jquery.validate.min.js" ></script>
<script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/scripts/plagins/maskInput.js" ></script>
<script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/scripts/plagins/slick.js" ></script>
    <!-- add you plagins js here -->

<!-- endbuild -->

<!-- build:js scripts/main.js -->
<script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/scripts/basic_scripts.js" ></script>
<script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/scripts/develop/develop_1.js" ></script>
</body>
<?php wp_footer();?>
</html>

